const express = require('express'); 
const mongoose = require('mongoose');
const requireDir = require("require-dir");

const porta = 3001

const app = express();  
app.use(express.json());

mongoose.connect('mongodb://localhost:27017/base',{useNewUrlParser: true});

requireDir('./src/models');


app.use('/api', require('./src/routes')); 

app.listen(porta, () => {
    console.log(`excutando na porta ${porta}`)
});