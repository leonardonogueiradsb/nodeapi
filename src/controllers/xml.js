const response = require('../helper/response')
const request = require('../helper/request')

module.exports = (obj) => {
    xml = request(obj)
    json = response(xml)
    return json
}
