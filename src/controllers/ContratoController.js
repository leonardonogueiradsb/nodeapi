const mongoose = require('mongoose'); 

const Contrato = mongoose.model('Contrato'); 

module.exports = {
    async index(req, res){
        const contratos = await Contrato.find(); 
        return res.json(contratos); 
    }, 

    async store(req, res){
        const contrato = await Contrato.create(req.body); 
        return res.json(contrato);
    }, 

    async show(req, res){
        const contrato = await Contrato.find({'cpf': req.params.id})
        return res.json(contrato); 
    },
}