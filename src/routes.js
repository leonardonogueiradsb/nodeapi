const express = require('express'); 
const routes = express.Router(); 
const Controller = require('./controllers/ContratoController');
const xml = require('./controllers/xml')

routes.get('/contratos', Controller.index);
routes.post('/contratos', Controller.store);  
routes.get('/contratos/:id', Controller.show)
routes.get('/xml', (req, res) => {
    res.send(xml(req.body))
})

module.exports = routes; 